
public class CollisionResult {

    // whether the objects collided
    public boolean collided;

    // copies of the objects involved
    public PhysicsBody a, b;

    // For collisions where the separating axis theorem was used
    // (smallestOverlapAxis will be null, otherwise). Helps with using 
    // "projection" style collision handling.
    public Vector2D smallestOverlapAxis;
    public double smallestOverlapAmount;

    // debugging - colliding polygon edges
    public int a_collided, b_collided;

    public CollisionResult(PhysicsBody a, PhysicsBody b, boolean collided) {
        this.a = a;
        this.b = b;
        this.collided = collided;
    }

    public CollisionResult(PhysicsBody a, PhysicsBody b, boolean collided,
                           Vector2D smallestOverlapAxis, double smallestOverlapAmount) {
        this(a, b, collided);
        this.smallestOverlapAxis = smallestOverlapAxis;
        this.smallestOverlapAmount = smallestOverlapAmount;
    }

    public CollisionResult(PhysicsBody a, PhysicsBody b, boolean collided,
                           Vector2D smallestOverlapAxis, double smallestOverlapAmount,
                           int a_collided, int b_collided) {
        this(a, b, collided, smallestOverlapAxis, smallestOverlapAmount);
        this.a_collided = a_collided;
        this.b_collided = b_collided;
    }
}
