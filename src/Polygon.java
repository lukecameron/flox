import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import processing.core.PApplet;
import processing.core.PImage;

// representation of a (convex) polygon
public class Polygon implements Iterable<Vector2D>, PhysicsBody, Drawable {
    // clockwise-ordered list of points
    private List<Vector2D> points;
    private SimplePhysicsBody p;

    // debug collision code
    public List<Boolean> collided;

    // partial constructor
    private Polygon(Vector2D pos) {
        points = new ArrayList<Vector2D>();
        p = new SimplePhysicsBody();
        p.pos(pos);
    }

    // triangle constructor (clockwise)
    public Polygon(Vector2D pos, Vector2D a, Vector2D b, Vector2D c) {
        this(pos);
        points.add(a);
        points.add(b);
        points.add(c);
    }

    // quad constructor (clockwise)
    public Polygon(Vector2D pos, Vector2D a, Vector2D b, Vector2D c, Vector2D d) {
        this(pos, a, b, c);
        points.add(d);
    }

    // iterable constructor
    public Polygon(Vector2D pos, Iterable<Vector2D> itPoints) {
        this(pos);
        for (Vector2D point : itPoints) {
            points.add(point);
        }
    }

    public Polygon(List <Vector2D> points, SimplePhysicsBody p) {
        this.points = points;
        this.p = p;
    }

    public Polygon move(Vector2D dp) {
        Polygon poly = new Polygon(points, p.move(dp));
        return poly;
    }

    @Override
    public CollisionResult isColliding(PhysicsBody c) {
        if (c instanceof Polygon) {
            return CollisionHandlers.polygonPolygon(this, (Polygon) c);
        } else {
            throw new RuntimeException("Polygon.isColliding with unknown type");
        }
    }

    @Override
    public Iterator<Vector2D> iterator() {
        // allow for-loop iteration
        return points.iterator();
    }

    public List<Vector2D> getPoints() {
        // trust client with internal list for efficiency
        return points;
    }

    public List<Vector2D> getGlobalPoints() {
        List<Vector2D> l = new ArrayList<Vector2D>();
        for (Vector2D point : points) {
            l.add(point.c().add(p.pos()));
        }
        return l;
    }

    @Override
    public Rect boundingRect() {
        Rect r = new Rect(points.get(0).x, points.get(0).y, points.get(0).x, points.get(0).y);
        for (Vector2D p : points) {
            r.ax = Math.min(r.ax, p.x);
            r.ay = Math.min(r.ay, p.y);
            r.bx = Math.max(r.bx, p.x);
            r.by = Math.max(r.by, p.y);
        }
        return r.move(p.pos().x, p.pos().y);
    }

    @Override
    public Vector2D pos() {
        return p.pos();
    }

    @Override
    public Vector2D vel() {
        return p.vel();
    }

    @Override
    public void pos(Vector2D v) {
        p.pos(v);
    }

    @Override
    public void vel(Vector2D v) {
        p.vel(v);
    }

    @Override
    public void integrateVel(double dt, Vector2D acc) {
        p.integrateVel(dt, acc);
    }

    @Override
    public void integratePos(double dt) {
        // note: ignore p.integratePos implementation
        p.pos(p.pos().add(p.vel().c().scale(dt)));
    }

    @Override
    public Polygon integratePosSweep(double dt) {
        List <Vector2D> points2 = new ArrayList <Vector2D> (points);
        Vector2D dp = p.vel().c().scale(dt);
        for (Vector2D pt : points) {
            points2.add(pt.c().add(dp));
        }

        List <Vector2D> sweep = new ArrayList <Vector2D> ();
        int first = 0, n = points2.size();
        // find leftmost point
        for (int i = 1; i < n; ++i) {
            if (points2.get(i).x < points2.get(first).x) {
                first = i;
            }
        }
        sweep.add(points2.get(first));

        int cur = first;
        // the loop condition is necessary due to occasional numerical error
        while (sweep.size() < points2.size()) {
            Vector2D curP = points2.get(cur);

            int next = (cur + 1) % n;
            for (int i = (next + 1) % n; i != cur; i = (i + 1) % n) {
                Vector2D e = points2.get(i).c().subtract(curP);
                Vector2D nextE = points2.get(next).c().subtract(curP);
                Vector2D nextNorm = new Vector2D(-nextE.y, nextE.x);

                double left = e.dot(nextNorm);
                if (left > 0) {
                    next = i;
                }
            }

            if (next == first) {
                break;
            }
            sweep.add(points2.get(next));
            cur = next;
        }

        /*
        for (Vector2D pt : points) {
            System.out.print(pt + " ");
        }
        System.out.print("  + " + dp + " -->   ");
        for (Vector2D pt : points2) {
            System.out.print(pt + " ");
        }
        System.out.print("  -->   ");
        for (Vector2D pt : sweep) {
            System.out.print(pt + " ");
        }
        System.out.println("");
        */

        return new Polygon(sweep, p.move(Vector2D.zero));
    }

    @Override
    public Vector2D selfForce() {
        return Vector2D.zero;
    }

    @Override
    public double mass() {
        return p.mass();
    }

    @Override
    public void mass(double m) {
        p.mass(m);
    }

    @Override
    public void draw(Window w, double dt) {
        if (boundingRect().intersect(w.screen)) {
            w.noFill();
            w.stroke(0);
            w.strokeWeight(1);
            w.beginShape();
            for (Vector2D v : points) {
                Vector2D displayP = v.c().add(p.pos()).worldToScreen(w);
                w.vertex((float) displayP.x, w.height - (float) displayP.y);
            }
            w.endShape(PApplet.CLOSE);
        }

        // debug collision code
        if (collided != null) {
            for (int i = 0; i < collided.size(); ++i) {
                if (collided.get(i)) {
                    Vector2D v1 = points.get(i).c().add(p.pos()).worldToScreen(w);
                    Vector2D v2 = points.get((i+1) % points.size()).c().add(p.pos()).worldToScreen(w);
                    w.stroke(0xffff0000);
                    w.line((float)v1.x, w.height - (float)v1.y, (float)v2.x, w.height - (float)v2.y);
                }
            }
        }
        Vector2D v1 = pos().c().worldToScreen(w);
        Vector2D v2 = pos().c().add(vel()).worldToScreen(w);
        w.stroke(0xff0000ff);
        w.line((float)v1.x, w.height - (float)v1.y, (float)v2.x, w.height - (float)v2.y);
    }

    @Override
    public boolean isDynamic() {
        return p.isDynamic();
    }

    public void setDynamic() {
        p.setDynamic();
    }

    public void store(StringBuffer buf) {
        buf.append("[p " + points.size() + " ");
        p.pos().store(buf);
        buf.append(" ");
        p.vel().store(buf);
        for(int i = 0; i < points.size(); ++i) {
            buf.append(" ");
            points.get(i).store(buf);
        }
        buf.append(" ]");
    }

    public static Polygon load(Scanner scanner) {
        String head = scanner.next();
    	if (!head.equals("[p")) {
    		throw new java.util.InputMismatchException();
        }
        return load(scanner, head);
    }
    
    public static Polygon load(Scanner scanner, String head) {
    	if (!head.equals("[p")) {
    		return null;
    	}
    	ArrayList <Vector2D> points = new ArrayList <Vector2D> ();
        int size = scanner.nextInt();

        Vector2D pos = Vector2D.load(scanner);
        Vector2D vel = Vector2D.load(scanner);

        for (int i = 0; i < size; ++i) {
            points.add(Vector2D.load(scanner));
        }

        if (!scanner.next().equals("]")) {
            throw new java.util.InputMismatchException();
        }

        Polygon p = new Polygon(pos, points);
        p.vel(vel);
        return p;
    }

	@Override
	public boolean isMagnetic() {
		return p.isMagnetic();
	}

	@Override
	public void setMagnetic(boolean setTo) {
		this.p.setMagnetic(setTo);
	}

	@Override
	public double getMagneticStrength() {
		return this.p.getMagneticStrength();
	}

	@Override
	public void setMagneticStrength(double setTo) {
		this.p.setMagneticStrength(setTo);
	}
}
