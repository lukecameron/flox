import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;

import controlP5.*;
import sun.audio.*;

import java.util.Set;

import java.util.ArrayList;

import javax.sound.sampled.*;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;

public class Window extends PApplet {
	
	public ControlP5 cp5;
	public MultiList ml;
	public ArrayList<MultiListButton> buttons;

    private Level currentLevel;
    static String levelFile = "scratch.flox";
    public String[] levels;
    public String currentLevelPath = "";
    private double timeSinceFinished;
    // seconds to wait before loading new level
    private static final double ADVANCE_LEVEL_PAUSE = 2.0;
    private static boolean playAudio = false;
    
    public Set<Integer> specials;
    public Set<Character> keys;
    
    enum Mode {
        Play, Menu
    }
    
    public Mode mode = Mode.Menu;

    // screen position in level-space
    public Rect screen;
    float previousWidth, previousHeight;

    public Window() {
        //testLevel = new Level(this);
        specials = new HashSet<Integer>();
        keys = new HashSet<Character>();
        screen = new Rect(0, 0, 20, 20);
    }
    
    public void controlEvent(ControlEvent theEvent) {
    	switch(theEvent.getController().getId()) {
    	case(1):
    		mode = Mode.Menu;
    	break;
    	case(2):
    		// pressing play will unpause the game
            if (currentLevel != null) {
    		mode = Mode.Play;
            }
    	break;
    	}
    	
    	int controllerId = theEvent.getController().getId();
    	if (controllerId >= 4) {
    		load("levels" + File.separator + theEvent.getController().getLabel() + ".flox");
    	}
    }

    public void draw() {
    	double dt = 1.0 / frameRate;
    	
        if (previousWidth != width || previousHeight != height) {
            screen.scaleCentered(width / previousWidth / (height / previousHeight), 1);
            previousWidth = width;
            previousHeight = height;
        }

        if (mode==Mode.Menu) {
            if (currentLevel == null) {
                background(150);
            }
            cp5.show();

        	// still tick the game so it can receive keyboard commands
        	if (currentLevel != null) currentLevel.tick(dt);

        } else {
        	// If unpause here then unpause in level
        	if (currentLevel != null) {
        		if (currentLevel.mode == Level.Mode.Paused) {
    				currentLevel.mode = Level.Mode.Game;
    			}
        		
        		if (currentLevel.isCompleted()) {
        			timeSinceFinished += dt;
        			
        			if (timeSinceFinished >= ADVANCE_LEVEL_PAUSE) {
        				System.out.println("time elapsed. attempting to load next level");
        				// find current level name in array
            			boolean foundl = false;
            			int i;
            			for (i = 0; i < levels.length; ++i) {
            				System.out.println (currentLevelPath.replace("levels/", "") + " == " + levels[i]);
            				if (levels[i].equals(currentLevelPath.replace("levels/", ""))) {
            					foundl = true;
            					break;
            				}
            			}
            			if (foundl && i < levels.length - 1) {
            				load("levels" + File.separator + levels[i + 1]);
            			}
            			timeSinceFinished = 0;
            			System.out.println("mode: " + this.mode);
        			}
        		}
        		
        		currentLevel.tick(dt);
        	}

		cp5.hide();
        }
    }
    
    public void initMenu() {
        cp5 = new ControlP5(this);
        //cp5.setFont(createFont("", 20));
        
        ml = cp5.addMultiList("Menu", 20, 70, 300, 30);
        buttons = new ArrayList<MultiListButton>();  
        
        Button bu = cp5.addButton("Resume",2, 20, 30, 300, 30);
        //buttons.add(bu);
        bu.setId(2);

        /*b = ml.add("Play level", 3);
        b.setId(3);
        buttons.add(b);*/
        
        int i = 4;
        levels = new File("levels").list();
        for (String s : levels) {
        	if (s.endsWith(".flox")) {
        		String name = s.substring(0,  s.indexOf(".flox"));
        		MultiListButton but = ml.add(name, i);
        		but.setLabel(name);
        		but.setId(i);
        		buttons.add(but);
        	}
        	i++;
        }

    }
    
    public void setup() {
        size(1268, 720, P2D);

        initMenu();

        if (frame != null) {
            frame.setResizable(true);
        }
        previousWidth = 1;
        previousHeight = 1;
        this.frameRate(40);

        // play the soundtrack once
        if (playAudio) {
        	String audiofile = "track1.wav";
            try {
    			InputStream in = new FileInputStream(audiofile);
    			AudioStream audioStream = new AudioStream(in);
    			AudioPlayer.player.start(audioStream);
    		} catch (FileNotFoundException e) {
    			e.printStackTrace();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
        }
        
    }
    
    public void load(String fileName) {
        System.out.println("loading " + fileName);
    	
    	java.util.Scanner scanner = null;
        try {
            scanner = new java.util.Scanner(new java.io.FileInputStream(fileName));
            currentLevel = new Level(this);
            currentLevel.load(this, scanner);
            currentLevel.setFile(fileName);
            currentLevelPath = fileName;
            currentLevel.mode = Level.Mode.Game;
            mode = Mode.Play;
        } catch(java.io.FileNotFoundException f) {
            System.out.println(fileName + " not found!");
        } catch(RuntimeException e) {
            System.out.println("Exception when loading " + fileName + ": " + e);
        }

        timeSinceFinished = 0;
    }
    
    public void save(String fileName) {
        StringBuffer buf = new StringBuffer();
        currentLevel.store(buf);

        try {
            java.io.FileWriter f = new java.io.FileWriter(fileName);
            f.write(buf.toString());
            f.close();
        } catch(java.io.IOException e) {
            System.out.println("Note: could not save to \"" + fileName + "\": " + e);
            return;
        }
        System.out.println("Note: saved current level to \"" + fileName + "\"");
    }

    @Override
    public void keyPressed() {
    	
        if (key == CODED) {
            specials.add(keyCode);
        } else {
            keys.add(key);
        }
        // processing exits when ESC is pressed. we override this behaviour.
        if (key == ESC) {
            key = 0;
        }
    }

    @Override
    public void keyReleased() {
        if (key == CODED) {
            specials.remove(keyCode);
        } else {
            keys.remove(key);
        }
    }
    
    PImage imageMirrorX(PImage img) {
        PImage imgM = createImage(img.width, img.height, ARGB);
        img.loadPixels();
        imgM.loadPixels();
        for (int y = 0; y < img.height; ++y) {
            for (int x = 0; x < img.width; ++x) {
                imgM.pixels[y * img.width + (img.width - x - 1)] = img.pixels[y * img.width + x];
            }
        }
        imgM.updatePixels();
        return imgM;
    }

    public static void main(String[] arg) {
        PApplet.main(new String[] { "Window" });
    }
    
    public static final String STORY_TEXT = "Legend has it that there lived a shepherd named Magnes about 4,000 years ago in an area of Northern Greece called Magnesia. While herding sheep one day, both the nails in his shoes and the metal tip of his staff suddenly became firmly stuck to a large black rock upon which he was standing. To find the source of this strange attraction, Magnes dug up the Earth and discovered lodestones - a naturally magnetized piece of the mineral magnetite which was subsequently named after either Magnesia or Magnes himself.\n\nUnfortunately, Magnes' has lost all his sheep in an unfortunate accident. Help Magnes use his new discovery of magnets to reclaim his lost sheep!";
    
}
