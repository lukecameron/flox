import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import processing.core.PImage;
import processing.core.PApplet;
import controlP5.*;

public class Level {
    private Window w;

    // level geometry
    private ArrayList <Tile> tiles;
    private int width = 100;
    private int height = 100;
    private Set<Tile> magneticTiles;
    private PImage backgroundTexture;

    // characters
    private Magnus magnus;
    private List <Polygon> dynamics;
    private List<Sheep> sheep;

    // physics manager
    private PhysicsManager physics;

    // level editing
    Editor editor;
    
    // what level am I?
    int myLevel;

    // text triggers
    private List <Trigger> triggers;
    ControlP5 cp5;
    Textarea triggerText;
    boolean triggerPaused = false;
    double triggerExpire;

    // try to transition to this screen rect
    Rect targetScreen;
    boolean viewportOverride;

    // go to targetScreen slowly for this many seconds
    // this is set when each level is first loaded
    double initialZoom;
    static final double INITIAL_ZOOM_TIME = 2.0;

    public static final int MAX_SPEED = 10;
    private static final int MOVE_SPEED = 35;
    private static final int JUMP_HEIGHT = 15;

    private static final double SHEEP_FIND_DISTANCE = 2;

    enum Mode {
        Paused, Game, Edit
    }

    public Mode mode = Mode.Game;

	private String fileName;

	private boolean completed;

    public Level(Window w) {
        this.w = w;
        cp5 = new ControlP5(w);

        editor = new Editor(w);

        // initialize a (mostly) empty level
        tiles = new ArrayList <Tile> (width * height);
        for (int i = 0; i < width * height; ++i) {
            tiles.add(null);
        }
        
        // ground tiles
        for (int y = 0; y < 4; ++y) {
            for (int x = 0; x < width; ++x) {
                int i = y * width + x;
                tiles.set(i, editor.tiles.get(0).move(V(x, y)));
            }
        }
        
        completed = false;
        
        // add magnetic tiles to set
        refreshMagneticTiles();
        
        dynamics = new ArrayList<Polygon>();
        
        // add magnus to the scene
        magnus = new Magnus(V(10, 10), w);
        magnus.setDynamic();
        dynamics.add(magnus);
        
        // init sheep array
        sheep = new ArrayList<Sheep> ();
        
        triggers = new ArrayList <Trigger> ();

        backgroundTexture = Tile.getTexture(w, "background.png");

        // set up physics
        physics = new PhysicsManager();
    }
    
    public void refreshMagneticTiles() {
    	magneticTiles = new HashSet<Tile>();
    	for (Tile t : tiles) {
    		if (t != null && t.isMagnetic()) {
    			magneticTiles.add(t);
    		}
    	}
    	System.out.println("detected " + magneticTiles.size() + " magnets.");
    }

    // shortcut
    private static Vector2D V(double x, double y) {
        return new Vector2D(x, y);
    }
    
    public boolean isCompleted() {
    	return completed;
    }

    void removeTrigger() {
        if (triggerText != null) {
            cp5.remove("trigger");
            triggerText = null;
            triggerPaused = false;
        }
    }

    void setTrigger(Trigger t) {
        triggerText = cp5.addTextarea("trigger");
        Rect r = triggerTextRect();
        triggerText
            .setPosition((int)r.ax, (int)(w.height - r.by))
            .setSize((int)(r.bx - r.ax), (int)(r.by - r.ay))
            .setFont(w.createFont("", 20))
            .setText(t.text);
        triggerPaused = t.forcePause;
        if (t.removeAfter > 0) {
            triggerExpire = t.removeAfter;
        } else {
            triggerExpire = Double.MAX_VALUE;
        }
    }

    public void tick(double dt) {
        handleKeys(dt);

        if (mode == Mode.Game) {
        	
            // check goal conditions
            checkGoalConditions();
        	
            // move camera
            if (!viewportOverride) {
                moveCameraTowardsMagnes(0.5);
            }
            // constrain camera to the game area
            if (targetScreen.ax < 0 && targetScreen.bx < width) {
                targetScreen.move(Math.min(width - targetScreen.bx, -targetScreen.ax), 0);
            } else if (targetScreen.bx > width && targetScreen.ax > 0) {
                targetScreen.move(-Math.min(targetScreen.bx - width, targetScreen.ax), 0);
            }
            if (targetScreen.ay < 0 && targetScreen.by < height) {
                targetScreen.move(0, Math.min(height - targetScreen.by, -targetScreen.ay));
            } else if (targetScreen.by > height && targetScreen.ay > 0) {
                targetScreen.move(0, -Math.min(targetScreen.by - height, targetScreen.ay));
            }

            initialZoom = Math.max(0, initialZoom - dt);
            // we interpolate the zoom speed
            w.screen.moveTowardsKeepingAspect(targetScreen, 0.5 * (1 - initialZoom / INITIAL_ZOOM_TIME));

            // trigger expiry
            if (triggerText != null) {
                triggerExpire -= dt;
                if (triggerExpire <= 0) {
                    removeTrigger();
                }
            }
        	
            if (!triggerPaused) {
                // magnes physics
                runMagnesPhysics(dt);
        	
                // game simulation
                physics.tick(dt, dynamics, tiles, width, height, magneticTiles);

                // detect if magnes on ground
                magnus.updateRestingOnPlatform(tiles, width, height);

                // detect triggers
                for (Trigger t : triggers) {
                    if (t.triggered) {
                        continue;
                    }
                    if (t.area.intersect(magnus.boundingRect())) {
                        if (t.once) {
                            t.triggered = true;
                        }
                        if (t.text.isEmpty()) {
                            removeTrigger();
                        } else {
                            setTrigger(t);
                        }
                        break;
                    }
                }
            }
            
            // draw level
            draw(dt);
        } else if (mode == Mode.Edit) {
            // select tile
        	
            for (char digit = '0'; digit <= '9'; ++digit) {
                if (w.keys.contains(digit)) {
                	int sel = (int) (digit - '0');
                	if (w.specials.contains(w.ALT) == true) {
                        sel += 9;
                	}
                    if (sel < editor.tiles.size()) {
                        editor.currentTile = sel;
                    }
                    break;
                }
            }
    	
            // hack: handle mouse input here
            Vector2D mouseWorldPos = new Vector2D(w.mouseX, w.height - w.mouseY).screenToWorld(w);
            int mouseX = (int) mouseWorldPos.x, mouseY = (int) mouseWorldPos.y;
            if (!w.mousePressed || mouseX < 0 || mouseX >= width || mouseY < 0 || mouseY >= height) {
                // do nothing
            } else if (w.mouseButton == w.LEFT) {
                // draw tiles
            	Tile newTile = editor.newTile(new Vector2D(mouseX, mouseY));
            	if (newTile.isMagnetic()) magneticTiles.add(newTile);
            	
            	// detect if replacing a magnet and in that case remove it from the magnet set
            	Tile ti = tiles.get(mouseY * width + mouseX);
            	if (ti != null && ti.isMagnetic()) magneticTiles.remove(ti);
            	tiles.set(mouseY * width + mouseX, newTile);
            } else if (w.mouseButton == w.RIGHT) {
                // erase tiles
            	Tile ti = tiles.get(mouseY * width + mouseX);
            	if (ti != null && ti.isMagnetic()) {
            		magneticTiles.remove(ti);
            	}
                tiles.set(mouseY * width + mouseX, null);
                
            }

            draw(dt);

            // draw grid
            w.strokeWeight(1);
            w.stroke(0x40ffffff);
            for (int y = (int)Math.floor(w.screen.ay); y <= Math.ceil(w.screen.by); ++y) {
                float sy = (float) ((y - w.screen.ay) / w.screen.height()) * w.height;
                w.line(0, w.height - sy, w.width, w.height - sy);
            }
            for (int x = (int)Math.floor(w.screen.ax); x <= Math.ceil(w.screen.bx); ++x) {
                float sx = (float) ((x - w.screen.ax) / w.screen.width()) * w.width;
                w.line(sx, 0, sx, w.height);
            }

            if (!w.mousePressed) {
                // hover current tile
                editor.newTile(new Vector2D(mouseX, mouseY)).draw(w, dt);
            }
        } else if (mode == Mode.Paused) {
        	
            draw(dt);
            w.noStroke();
            w.fill(0x80808080);
            w.rect(0, 0, w.width, w.height);
            
            // bring up the pause menu
        	w.mode = Window.Mode.Menu;
        }
    	
    }

    private void checkGoalConditions() {
    	
    	// find new sheep
    	for (Sheep s : sheep) {
    		if (magnus.pos().distanceTo(s.pos()) < SHEEP_FIND_DISTANCE) {
    			s.setFound(true);
    		}
    	}
    	
    	// see if all sheep are found
    	boolean found = true;
    	for (Sheep s : sheep) {
    		found = found && s.isFound();
    	}
    	
    	completed = found;
    	if (completed) {
    		magnus.vel().zero();
    	}
    	
    	//TODO:print
    	
	}

	private void runMagnesPhysics(double dt) {
    	// friction
            if (magnus.onPlatform() && magnus.isMoving() == false) {
    		magnus.vel().x = magnus.vel().x * (1 - 10 * dt);
    		
    		// limit x speed
        	
    	}
    	magnus.vel().limitX(MAX_SPEED);
	}

	private void moveCameraTowardsMagnes(double speed) {
        Rect magnesRect = magnus.boundingRect();
        final double border = 0.4; // 0..0.5
        double left = targetScreen.ax + targetScreen.width() * border;
        double right = targetScreen.bx - targetScreen.width() * border;
        double bottom = targetScreen.ay + targetScreen.height() * border;
        double top = targetScreen.by - targetScreen.height() * border;
        if (left > magnesRect.bx) {
            targetScreen.move((magnesRect.bx - left) * speed, 0);
        } else if (right < magnesRect.ax) {
            targetScreen.move((magnesRect.ax - right) * speed, 0);
        }
        if (bottom > magnesRect.by) {
            targetScreen.move(0, (magnesRect.by - bottom) * speed);
        } else if (top < magnesRect.ay) {
            targetScreen.move(0, (magnesRect.ay - top) * speed);
        }
	}

	private void handleKeys(double dt) {
        if (w.keys.contains('s') && fileName != null) {
            StringBuffer buf = new StringBuffer();
            store(buf);

            try {
                java.io.FileWriter f = new java.io.FileWriter(fileName);
                f.write(buf.toString());
                f.close();
            } catch(java.io.IOException e) {
                System.out.println("Note: could not save to \"" + fileName + "\": " + e);
                return;
            }

            System.out.println("Note: saved current level to \"" + fileName + "\"");
            w.keys.remove('s');
        }

        if (w.specials.contains(w.SHIFT)) {
            if (w.specials.contains(w.RIGHT)) {
                targetScreen.move(targetScreen.width() * 0.5 * dt, 0.0);
                viewportOverride = true;
            }
            if (w.specials.contains(w.LEFT)) {
                targetScreen.move(-targetScreen.width() * 0.5 * dt, 0.0);
                viewportOverride = true;
            }
            if (w.specials.contains(w.UP)) {
                targetScreen.move(0.0, targetScreen.height() * 0.5 * dt);
                viewportOverride = true;
            }
            if (w.specials.contains(w.DOWN)) {
                targetScreen.move(0.0, -targetScreen.height() * 0.5 * dt);
                viewportOverride = true;
            }
        }

        switch (mode) {
        case Game:
            if (w.keys.contains(' ')) {
                // leave a trigger if there is one
                if (triggerText != null) {
                    removeTrigger();
                }
            } else if (w.keys.contains(w.ESC)) {
                // go to pause menu
                mode = Mode.Paused;
                w.mode = Window.Mode.Menu;
                w.keys.remove(w.ESC);
            } else if (w.keys.contains('e')) {
                mode = Mode.Edit;
                w.keys.remove('e');
            } else if (w.keys.contains('p')) {
                mode = Mode.Paused;
                w.mode = Window.Mode.Menu;
                w.keys.remove('p');
            } else if (!completed && !triggerPaused) {
                // player movement
            	magnus.setMoving(false);
                if (!w.specials.contains(w.SHIFT)) {
                    if (w.specials.contains(w.RIGHT)) {
                        viewportOverride = false;
                        magnus.setMoving(true);
                        if (magnus.onPlatform()) {
                            magnus.vel().add(new Vector2D(MOVE_SPEED * dt, 0));
                        }
                        else {
                            if (magnus.vel().x < MAX_SPEED) {
                                magnus.vel().add(new Vector2D(MOVE_SPEED * dt, 0));
                            }
                        }
                    }
                    if (w.specials.contains(w.LEFT)) {
                        viewportOverride = false;
                        magnus.setMoving(true);
                        if (magnus.onPlatform()) {
                            magnus.vel().add(new Vector2D(-MOVE_SPEED * dt, 0));
                        }
                        else {
                            if (magnus.vel().x > -MAX_SPEED) {
                                magnus.vel().add(new Vector2D(-MOVE_SPEED * dt, 0));
                            }
                        }
                    }
                    if (w.specials.contains(w.UP)) {
                        viewportOverride = false;
                	
                        if (magnus.onPlatform()) {
                            magnus.vel().y = JUMP_HEIGHT;
                            w.specials.remove(w.UP);
                        }
                    }
                }
                
                // Change Magnus Polarity
                if (w.keys.contains('q') || w.keys.contains('w')) {
                    viewportOverride = false;
                }

                if (w.keys.contains('q') && w.keys.contains('w')) {
                	// Do nothing
                	magnus.setMagnetic(false);
                } else if(w.keys.contains('q')){
                	magnus.setMagnetic(true);
                	magnus.setMagneticStrength(-1);
                } else if(w.keys.contains('w')){
                	magnus.setMagnetic(true);
                	magnus.setMagneticStrength(1);
                } else {
                	magnus.setMagnetic(false);
                }
            }
            break;
        case Edit:
            if (w.keys.contains('e')) {
                mode = Mode.Game;
                w.keys.remove('e');
            }
            if (w.specials.contains(w.RIGHT)) {
                w.screen.move(w.screen.width() * 0.5 * dt, 0.0);
            }
            if (w.specials.contains(w.LEFT)) {
                w.screen.move(-w.screen.width() * 0.5 * dt, 0.0);
            }
            if (w.specials.contains(w.UP)) {
                w.screen.move(0.0, w.screen.height() * 0.5 * dt);
            }
            if (w.specials.contains(w.DOWN)) {
                w.screen.move(0.0, -w.screen.height() * 0.5 * dt);
            }
            break;
        case Paused:
            if (w.keys.contains('p')) {
                mode = Mode.Game;
                w.mode = Window.Mode.Play;
                w.keys.remove('p');
            } else if (w.keys.contains(w.ESC)) {
                mode = Mode.Game;
                w.mode = Window.Mode.Play;
                w.keys.remove(w.ESC);
            }
            break;
        }
        if (w.keys.contains('-')) {
            targetScreen.scaleCentered(1 + dt, 1 + dt);
            if (targetScreen.width() > width) {
                targetScreen.scaleCentered(width/targetScreen.width(), width/targetScreen.width());
            }
            if (targetScreen.height() > height) {
                targetScreen.scaleCentered(height/targetScreen.height(), height/targetScreen.height());
            }
        }
        if (w.keys.contains('=') || w.keys.contains('+')) {
            targetScreen.scaleCentered(1 / (1 + dt), 1 / (1 + dt));
            if (targetScreen.width() < 1) {
                targetScreen.scaleCentered(1/targetScreen.width(), 1/targetScreen.width());
            }
            if (targetScreen.height() < 1) {
                targetScreen.scaleCentered(1/targetScreen.height(), 1/targetScreen.height());
            }
        }
    }

    private void draw(double dt) {
        w.background(150);

        // not strictly necessary as this gets done on every PApplet draw() call.
        // do anyway, just for completeness.
        w.pushMatrix();
        
        // Draw the background
        w.noStroke();
        w.textureMode(w.IMAGE);
        w.beginShape();
        w.texture(backgroundTexture);
        
        w.vertex(0, 0, 0, 0);
        w.vertex(w.width, 0, backgroundTexture.width, 0);
        w.vertex(w.width, w.height, backgroundTexture.width, backgroundTexture.height);
        w.vertex(0, w.height, 0, backgroundTexture.height);
        
        w.endShape(PApplet.CLOSE);
        
        // draw objects
        for (Drawable d : tiles) {
            if (d != null) {
                d.draw(w, dt);
            }
        }
        for (Drawable d : dynamics) {
            d.draw(w, dt);
        }

        // draw triggerText background
        if (triggerText != null) {
            w.stroke(255, 255, 255);
            w.strokeWeight(1);
            w.fill(128, 128, 128, 128);
            Rect r = triggerTextRect();
            w.rectMode(w.CORNERS);
            w.rect((float)r.ax, (float)(w.height - r.by), (float)r.bx, (float)(w.height - r.ay));
        }

        w.popMatrix();
    }

    public void store(StringBuffer buf) {
        buf.append("floxv0\n");
        buf.append("level:\n");
        buf.append(width + " " + height + "\n");
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                Tile t = tiles.get(y * width + x);
                if (t == null) {
                    buf.append("-");
                } else {
                    buf.append(t.getTileNumber());
                }
                if (x+1 < width) {
                    buf.append(" ");
                }
            }
            buf.append("\n");
        }

        buf.append("player: ");
        magnus.store(buf);
        buf.append("\n");

        buf.append("sheep: ");
        sheep.get(0).store(buf);
        buf.append("\n");
        
        buf.append(triggers.size() + " triggers:\n");
        for (Trigger t : triggers) {
            t.store(buf);
            buf.append("\n");
        }
    }

    static void scan(java.util.Scanner scanner, String str) {
        if (!scanner.next().equals(str)) {
            throw new java.util.InputMismatchException();
        }
    }

    public void load(Window w, java.util.Scanner scanner) {
        scan(scanner, "floxv0");

        scan(scanner, "level:");
        int width = scanner.nextInt();
        int height = scanner.nextInt();

        if (width < 0 || height < 0) {
            throw new java.util.InputMismatchException();
        }

        ArrayList <Tile> tiles = new ArrayList <Tile> ();
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
            	String s = scanner.next();
            	if (s.equals("-")) {
            		tiles.add(null);
            	} else {
            		int i = Integer.parseInt(s);
            		tiles.add(editor.tiles.get(i).move(V(x, y)));
            	}
            }
        }

        scan(scanner, "player:");
        Magnus magnus = Magnus.load(scanner, w);
        magnus.setDynamic();
        scan(scanner, "sheep:");
        Sheep loadedSheep = Sheep.load(scanner, w);
        sheep.add(loadedSheep);

        ArrayList <Polygon> dynamics = new ArrayList <Polygon> ();
        dynamics.add(magnus);
        dynamics.add(loadedSheep);
        
        int numTriggers = scanner.nextInt();
        if (numTriggers < 0) {
            throw new java.util.InputMismatchException();
        }
        scan(scanner, "triggers:");
        List <Trigger> triggers = new ArrayList <Trigger> ();
        for (int i = 0; i < numTriggers; ++i) {
            triggers.add(Trigger.load(scanner));
        }

        this.width = width;
        this.height = height;
        this.tiles = tiles;
        this.magnus = magnus;
        this.dynamics = dynamics;
        this.triggers = triggers;

        // show entire level
        Rect newScreen = new Rect(0, 0, width, height);
        double screenAspect = w.screen.width() / w.screen.height();
        double newAspect = newScreen.width() / newScreen.height();
        if (newAspect > screenAspect) {
            newScreen.scaleCentered(1, newAspect / screenAspect);
        } else if (newAspect < screenAspect) {
            newScreen.scaleCentered(screenAspect / newAspect, 1);
        }
        w.screen = newScreen;
        viewportOverride = false;

        // target screen is around the player
        targetScreen = newScreen.clone();
        Rect magnesRect = magnus.boundingRect();
        double xscale = targetScreen.width() / magnesRect.width();
        double yscale = targetScreen.height() / magnesRect.height();
        double scale = Math.min(xscale, yscale) / 10;
        targetScreen.scaleCentered(1/scale, 1/scale);
        moveCameraTowardsMagnes(1);
        initialZoom = INITIAL_ZOOM_TIME;
        
        refreshMagneticTiles();
        
        if (mode == Mode.Game) {
            mode = Mode.Paused;
        }

        removeTrigger();
    }

    Rect triggerTextRect() {
        return new Rect(0, 0, w.width, w.height * 0.2);
    }

	public void setFile(String fileName) {
		this.fileName = fileName;
	}
}
