import java.util.List;
import java.util.ArrayList;
import java.util.Set;

// the class that integrates the positions and velocities of objects and 
// decides what to do when collisions happen
public class PhysicsManager {
	
	private static final double MAGNETIC_CONSTANT = 2500;

    private Vector2D gravity = new Vector2D(0, -40);

    public PhysicsManager() {
    }

    public void tick(double dt, List<? extends PhysicsBody> objects,
                     ArrayList <Tile> tiles, int width, int height,
                     Set <Tile> magnets) {
        List <Vector2D> orig_pos = new ArrayList <Vector2D> ();

        // update velocities with fixed dt
        for (PhysicsBody p : objects) {
            orig_pos.add(p.pos().c());

            // sum forces on object (re-use vector for summing)
            Vector2D force = new Vector2D(0,0);
            force.zero().add(p.selfForce());;

            // calculate and add force on object due to magnets
            if (p.isMagnetic()) {
            	Vector2D magneticResult = new Vector2D();
            	// find the overall force for all magnets in the level
            	for (Tile t : magnets) {
            		magneticResult.add(calculateMagneticForce(p, t));
            		//System.out.println("accumulating magnetic force: " + magneticResult);
            	}
            	force.add(magneticResult);
            	//System.out.println("magnetic force: " + force);
            }
            
            // convert to acceleration (i.e. take mass into account)
            force.scale(1 / p.mass());

            // now that force is actually acceleration, add gravity
            force.add(gravity);

            // update velocity
            p.integrateVel(dt, force);

            ((Polygon)p).collided = new ArrayList <Boolean> ();
            for (int i = 0; i < ((Polygon)p).getPoints().size(); ++i) {
                ((Polygon)p).collided.add(false);
            }
        }

        for (Tile t : tiles) {
            if (t != null) {
                t.poly.collided = null;
            }
        }

        // try moving objects with dynamic dt
        double t_eps = 1e-3;
        moveObjects(dt, t_eps, objects, tiles, width, height);

        // sometimes fixVelocity doesn't work, constrain velocity here
        for (int i = 0; i < objects.size(); ++i) {
            Vector2D trueVel = objects.get(i).pos().c().subtract(orig_pos.get(i)).scale(1 / dt);
            if (trueVel.length() < objects.get(i).vel().length()) {
                objects.get(i).vel(trueVel);
            }
        }
    }
    
    private Vector2D calculateMagneticForce(PhysicsBody p, Tile t) {
    	Vector2D result = p.pos().c().subtract(t.pos()).normalise();
    	
    	// Do math here
    	double dist = p.pos().distanceTo(t.pos());
    	
    	// can't have distance 0
    	dist = Math.max(0.001, dist);
    	//System.out.println("dist: " + dist);
    	
    	
    	double force = (MAGNETIC_CONSTANT * p.getMagneticStrength() * t.fieldStrength()) / (dist*dist*dist);
    	if (Double.isNaN(force)) {
    		force = 0;
    	}
    	
    	// limit force
    	double forceLimit = 40;
    	if (force >= 0) force = Math.min(force, forceLimit);
    	else force = Math.max(force, -forceLimit);
    	
    	
    	
    	return result.scale(force);
    }

    private void moveObjects(double dt, double t_eps,
                             List <? extends PhysicsBody> objects,
                             ArrayList <Tile> tiles, int width, int height) {
        // try moving all objects
        List <PhysicsBody> swept_objects = new ArrayList <PhysicsBody> ();
        List <Vector2D> orig_pos = new ArrayList <Vector2D> ();
        for (PhysicsBody b : objects) {
            orig_pos.add(b.pos().c());
            swept_objects.add(b.integratePosSweep(dt));
            b.integratePos(dt);
        }

        boolean basecase = dt <= t_eps;

        // check collisions.
        // TODO: check sweeps of moving objects
        boolean any_collided = false;
        ArrayList <Boolean> stop = new ArrayList <Boolean> ();
        for (int i = 0; i < objects.size(); ++i) {
            PhysicsBody b = objects.get(i);
            PhysicsBody swept_b = swept_objects.get(i);

            boolean b_collided = false;

            for (int j = 0; j < objects.size(); ++j) {
                PhysicsBody b2 = objects.get(j);
                PhysicsBody swept_b2 = swept_objects.get(j);
                if (b == b2) {
                    continue;
                }

                CollisionResult c = swept_b.isColliding(swept_b2);
                if (c.collided) {
                    // resolve the collision
                    if (basecase) {
                        CollisionHandlers.fixVelocity(c);
                        
                        if (b.isDynamic()) {
                            b.vel(swept_b.vel());
                        }
                        if (b2.isDynamic()) {
                            b2.vel(swept_b2.vel());
                        }
                        if (c.a_collided >= 0 && c.a_collided < ((Polygon)b).collided.size()) {
                            ((Polygon)b).collided.set(c.a_collided, true);
                        }
                        if (c.b_collided >= 0 && c.b_collided < ((Polygon)b2).collided.size()) {
                            ((Polygon)b2).collided.set(c.b_collided, true);
                        }
                    }
                    b_collided = true;
                    break;
                }
            }

            Rect br = swept_b.boundingRect();
            br.ax = Math.floor(br.ax);
            br.ay = Math.floor(br.ay);
            br.bx = Math.ceil(br.bx);
            br.by = Math.ceil(br.by);

            int xstart = Math.max(0, (int)br.ax);
            int xend = Math.min(width, (int)br.bx);
            int ystart = Math.max(0, (int)br.ay);
            int yend = Math.min(height, (int)br.by);
            for (int y = ystart; y < yend && !b_collided; ++y) {
                for (int x = xstart; x < xend && !b_collided; ++x) {
                    Tile t = tiles.get(y * width + x);
                    if (t == null) continue;

                    CollisionResult c = swept_b.isColliding(t.poly);
                    if (c.collided) {
                        if (basecase) {
                            CollisionHandlers.fixVelocity(c);
                            if (b.isDynamic()) {
                                b.vel(swept_b.vel());
                            }
                            if (c.a_collided >= 0 && c.a_collided < ((Polygon)b).collided.size()) {
                                ((Polygon)b).collided.set(c.a_collided, true);
                            }
                            if (c.b_collided >= 0) {
                                if (t.poly.collided == null) {
                                    t.poly.collided = new ArrayList <Boolean> ();
                                    for (int j = 0; j < t.poly.getPoints().size(); ++j) {
                                        t.poly.collided.add(false);
                                    }
                                }
                                t.poly.collided.set(c.b_collided, true);
                            }
                        }
                        b_collided = true;
                    }
                }
            }

            stop.add(b_collided);
            if (b_collided) {
                any_collided = true;
            }
        }

        if (!any_collided) {
            // done
            return;
        }
        if (basecase) {
            // don't recurse any more, just rollback collisions and return
            for (int i = 0; i < objects.size(); ++i) {
                if (stop.get(i)) {
                    // restore to original position
                    //System.out.println("Roll back obj[" + i + "] from " + objects.get(i).pos() + " to " + orig_pos.get(i));
                    objects.get(i).pos(orig_pos.get(i));
                }
            }
        } else {
            // rollback and recurse on smaller dt
            for (int i = 0; i < objects.size(); ++i) {
                // restore to original position
                objects.get(i).pos(orig_pos.get(i));
            }

            for (int i = 0; i < 2; ++i) {
                moveObjects(dt/2, t_eps, objects, tiles, width, height);
            }
        }
    }
}
