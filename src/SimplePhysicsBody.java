import java.util.List;


public class SimplePhysicsBody implements PhysicsBody {

    private Vector2D pos, vel;
    private double mass;
    private boolean dynamic;
    
    private boolean magnetic;
    private double magneticStrength;

    public SimplePhysicsBody() {
        pos = new Vector2D(0, 0);
        vel = new Vector2D(0, 0);
        mass = 1;
        dynamic = false;
        magnetic = false;
        magneticStrength = 0;
    }

    public SimplePhysicsBody move(Vector2D dv) {
        SimplePhysicsBody pb = new SimplePhysicsBody();
        pb.pos = new Vector2D(pos).add(dv);
        pb.vel = vel.c();
        pb.mass = mass;
        pb.dynamic = dynamic;
        return pb;
    }

    @Override
    public Vector2D pos() {
        return pos;
    }

    @Override
    public Vector2D vel() {
        return vel;
    }

    @Override
    public void pos(Vector2D p) {
        pos = p;
    }

    @Override
    public void vel(Vector2D v) {
        vel = v;
    }

    @Override
    public Rect boundingRect() {
        return new Rect(pos.x, pos.y, pos.x, pos.y);
    }

    @Override
    public void integrateVel(double dt, Vector2D a) {
        // euler for now, make better later
        vel.add(a.c().scale(dt)).limitLength(30);
    }

    // Due to how PhysicsManager currently works,
    // this should not be changed from Eulerian integration
    @Override
    public void integratePos(double dt) {
        pos.add(vel.c().scale(dt));
    }

    @Override
    public SimplePhysicsBody integratePosSweep(double dt) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Vector2D selfForce() {
        return Vector2D.zero;
    }

    @Override
    public double mass() {
        return mass;
    }

    @Override
    public void mass(double m) {
        mass = m;
    }

    // SimplePhysicsBody is very simple.
    @Override
    public CollisionResult isColliding(PhysicsBody c) {
        return new CollisionResult(this, c, false);
    }

    @Override
    public boolean isDynamic() {
        return dynamic;
    }

    public void setDynamic() {
        dynamic = true;
    }

	@Override
	public boolean isMagnetic() {
		return magnetic;
	}

	@Override
	public void setMagnetic(boolean setTo) {
		this.magnetic = setTo;
	}

	@Override
	public double getMagneticStrength() {
		return this.magneticStrength;
	}

	@Override
	public void setMagneticStrength(double setTo) {
		this.magneticStrength = setTo;
	}
}
