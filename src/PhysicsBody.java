import java.util.List;

public interface PhysicsBody {
    public Vector2D pos();
    public Vector2D vel();
    public void pos(Vector2D p);
    public void vel(Vector2D v);

    public Rect boundingRect();

    public void integrateVel(double dt, Vector2D acc);
    public void integratePos(double dt);
    public PhysicsBody integratePosSweep(double dt);

    // get a force inflicted by the object itself (e.g. a rocket, running due to AI, keyboard control)
    public Vector2D selfForce();
    public double mass();
    public void mass(double m);

    // collision
    public CollisionResult isColliding(PhysicsBody c);

    // needed to account for the case where two dynamic objects collide
    // (they both end up getting a collision event)
    public boolean isDynamic();
    
    public boolean isMagnetic();
    public void setMagnetic(boolean setTo);
    public double getMagneticStrength();
    public void setMagneticStrength(double setTo);
}
