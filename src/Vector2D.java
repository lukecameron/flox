/*
 * A stateful 2d vector class. All operations modify the current vector to facilitate reuse.
 */

public class Vector2D {
    public double x, y;

    public static final Vector2D zero = new Vector2D();
    public static final Vector2D i = new Vector2D(1, 0);
    public static final Vector2D j = new Vector2D(0, 1);

    public Vector2D() { }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D(Vector2D cloneFrom) {
        x = cloneFrom.x;
        y = cloneFrom.y;
    }

    public Vector2D clone() {
        return new Vector2D(this);
    }

    // copy quickly
    public Vector2D c() {
        return clone();
    }

    public Vector2D zero() {
        x = 0;
        y = 0;
        return this;
    }

    // float accessors
    public float x() {
        return (float) x;
    }

    public float y() {
        return (float) y;
    }

    public Vector2D assign(Vector2D assignFrom) {
        x = assignFrom.x;
        y = assignFrom.y;
        return this;
    }

    public Vector2D add(Vector2D a) {
        x += a.x;
        y += a.y;
        return this;
    }

    public Vector2D subtract(Vector2D a) {
        x -= a.x;
        y -= a.y;
        return this;
    }

    public Vector2D scale(double f) {
        x *= f;
        y *= f;
        return this;
    }

    public Vector2D scale(double f, double g) {
        x *= f;
        y *= g;
        return this;
    }

    public Vector2D interpolate(Vector2D a, double t) {
        x += t * (a.x - x);
        y += t * (a.y - y);
        return this;
    }

    public double abs() {
        return x * x + y * y;
    }

    public double length() {
        return Math.sqrt(abs());
    }

    public Vector2D normalise() {
        scale(1.0d / length());
        return this;
    }

    public double dot(Vector2D d) {
        return x * d.x + y * d.y;
    }

    public Vector2D rotate(double radians) {
        x = Math.cos(radians) * x - Math.sin(radians) * y;
        y = Math.sin(radians) * x + Math.cos(radians) * y;
        return this;
    }

    // the projection of this on b
    public Vector2D project(Vector2D b) {
        double dot = dot(b);
        x = (dot / (b.x*b.x + b.y*b.y)) * b.x;
        y = (dot / (b.x*b.x + b.y*b.y)) * b.y;
        return this;
    }

    public Vector2D rightHandNormal() {
        double tempX = x;
        x = y;
        y = -tempX;
        normalise();
        return this;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }
    
    public Vector2D limitLength(double limit) {
    	double len = length();
    	if (len > limit) {
    		scale(limit / len);
    	}
    	return this;
    }

    public Vector2D negate() {
        x = -x;
        y = -y;
        return this;
    }

    Vector2D worldToScreen(Window w) {
        return c().add(new Vector2D(-w.screen.ax, -w.screen.ay))
            .scale(w.width / w.screen.width(), w.height / w.screen.height());
    }
    Vector2D screenToWorld(Window w) {
        return c().scale(w.screen.width() / w.width, w.screen.height() / w.height)
            .add(new Vector2D(w.screen.ax, w.screen.ay));
    }

    public void store(StringBuffer buf) {
        buf.append(x + " " + y);
    }

    static public Vector2D load(java.util.Scanner scanner) {
        double x = scanner.nextDouble();
        double y = scanner.nextDouble();
        return new Vector2D(x, y);
    }

	public Vector2D reverse() {
		double temp = y;
		y = x;
		x = temp;
		return this;
	}

	public Vector2D subtract(double d) {
		x -= d;
		y -= d;
		return this;
	}

	public Vector2D limitX(double d) {
		if (x >= 0)
			x = Math.min(x, d);
		else
			x = Math.max(x, -d);
		return this;
	}

	public double distanceTo(Vector2D v) {
		return Math.sqrt((v.x - x) * (v.x - x) + (v.y - y) * (v.y - y));
	}
}
