import java.util.List;

import processing.core.PApplet;

public class CollisionHandlers {
    public static CollisionResult polygonPolygon(Polygon a, Polygon b) {
        if (!a.boundingRect().intersect(b.boundingRect())) {
            return new CollisionResult(a, b, false, null, 0);
        }

        List<Vector2D> pointsA = a.getGlobalPoints();
        List<Vector2D> pointsB = b.getGlobalPoints();

        boolean overlapGlobal = true;
        Vector2D smallestOverlapAxis = null;
        double smallestOverlapAmount = Double.MAX_VALUE;
        int a_collide = -1, b_collide = -1;

        // for each edge of first polygon, check overlap
        for (int i = 0; i < pointsA.size(); ++i) {
            Vector2D e1 = pointsA.get(i);
            Vector2D e2 = pointsA.get((i + 1) % pointsA.size());

            // this is the right-hand normal of e2 - e1, which points inside the polygon since it is in clockwise order
            Vector2D eNormal = e2.c().subtract(e1).rightHandNormal();

            // find minimum and maximum vertices of a along this axis.
            double minA = Double.MAX_VALUE, maxA = -Double.MAX_VALUE;
            for (Vector2D vertex : pointsA) {
                double dot = vertex.c().subtract(e1).dot(eNormal);
                if (dot < minA) minA = dot;
                if (dot > maxA) maxA = dot;
            }

            // find minimum and maximum vertices of b along this axis.
            double minB = Double.MAX_VALUE, maxB = -Double.MAX_VALUE;
            for (Vector2D vertex : pointsB) {
                double dot = vertex.c().subtract(e1).dot(eNormal);
                if (dot < minB) minB = dot;
                if (dot > maxB) maxB = dot;
            }

            // are polygons intersecting along this axis?
            double overlapAmount = Math.min(maxA, maxB) - Math.max(minA, minB);
            boolean overlap = overlapAmount > 0;

            if (overlap) {
                if (overlapAmount < smallestOverlapAmount) {
                    smallestOverlapAmount = overlapAmount;
                    smallestOverlapAxis = eNormal;
                    a_collide = i;
                }
            }
            else {
                overlapGlobal = false;
                // we can early-exit, since we've found an axis 
                // along which polies don't intersect (SAP)
                break;
            }
        }

        boolean foundOnA = overlapGlobal;

        // same for second polygon
        double bSmallestOverlap = Double.MAX_VALUE;
        for (int i = 0; i < pointsB.size(); ++i) {
            Vector2D e1 = pointsB.get(i);
            Vector2D e2 = pointsB.get((i + 1) % pointsB.size());

            // this is the right-hand normal of e2 - e1, which points inside the polygon since it is in clockwise order
            Vector2D eNormal = e2.c().subtract(e1).rightHandNormal();

            // find minimum and maximum vertices of a along this axis.
            double minA = Double.MAX_VALUE, maxA = -Double.MAX_VALUE;
            for (Vector2D vertex : pointsA) {
                double dot = vertex.c().subtract(e1).dot(eNormal);
                if (dot < minA) minA = dot;
                if (dot > maxA) maxA = dot;
            }

            // find minimum and maximum vertices of b along this axis.
            double minB = Double.MAX_VALUE, maxB = -Double.MAX_VALUE;
            for (Vector2D vertex : pointsB) {
                double dot = vertex.c().subtract(e1).dot(eNormal);
                if (dot < minB) minB = dot;
                if (dot > maxB) maxB = dot;
            }

            // are polygons intersecting along this axis?
            double overlapAmount = Math.min(maxA, maxB) - Math.max(minA, minB);
            boolean overlap = overlapAmount > 0;

            if (overlap) {
                if (overlapAmount < bSmallestOverlap) {
                    bSmallestOverlap = overlapAmount;
                    b_collide = i;
                }
                if (overlapAmount < smallestOverlapAmount) {
                    smallestOverlapAmount = overlapAmount;
                    smallestOverlapAxis = eNormal;
                }
            }
            else {
                overlapGlobal = false;
                // we can early-exit, since we've found an axis 
                // along which polies don't intersect (SAP)
                break;
            }
        }

        // the smallestOverlapAxis is assumed to point from a to b, so negate it
        // if we found it on b
        if (overlapGlobal && !foundOnA) smallestOverlapAxis.negate(); 

        return new CollisionResult(a, b, overlapGlobal, smallestOverlapAxis, smallestOverlapAmount, a_collide, b_collide);
    }

    public static void fixVelocity(CollisionResult c) {
        if (c.a instanceof Polygon && c.b instanceof Polygon) {
            if (c.smallestOverlapAxis == null) {
                throw new RuntimeException("fixVelocity only implemented with overlapping axis");
            }

            //System.out.println("overlapaxis = " + c.smallestOverlapAxis);

            // TODO: something better.
            // For now, just set the velocity in the direction of the collision axis to 0.
            if (c.a.isDynamic()) {
                //System.out.print("old a.vel = " + c.a.vel() + ", ");
                c.a.vel().subtract(c.smallestOverlapAxis.c().scale(c.a.vel().dot(c.smallestOverlapAxis) / c.smallestOverlapAxis.abs()));
                //System.out.println("new a.vel = " + c.a.vel());
            }
            if (c.b.isDynamic()) {
                //System.out.print("old b.vel = " + c.b.vel() + ", ");
                c.b.vel().subtract(c.smallestOverlapAxis.c().scale(c.b.vel().dot(c.smallestOverlapAxis) / c.smallestOverlapAxis.abs()));
                //System.out.println("new b.vel = " + c.b.vel());
            }
        } else {
            throw new RuntimeException("fixVelocity only implemented for Polygon");
        }
    }
}
