/*
 * Level editing "toolbox".
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import processing.core.PImage;

public class Editor {
    // each tile is in the square [0,1] × [0,1]
    public ArrayList <Tile> tiles;
    public List<Integer> tileTextures;
    public List<String> texturePaths;

    int currentTile;

    private Window w;

    Editor(Window w) {
        this.w = w;

        tiles = new ArrayList <Tile> ();

        currentTile = 0;

        // Add all tiles from tiles.def file
        File file = new File("tiles.def");
        Scanner scanner = null;
        try {
			scanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        
        if (scanner != null) {
        	int count = 0;
        	while (scanner.hasNext()) {
        		Tile t = Tile.load(w, scanner);
        		t.setTileNumber(count);
        		tiles.add(t);
        		scanner.nextLine();
        		++count;
        	}
        	scanner.close();
        }
    }

    Tile newTile(Vector2D offset) {
    	return tiles.get(currentTile).move(offset);
    }
}
