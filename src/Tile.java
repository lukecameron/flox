import processing.core.PApplet;
import processing.core.PImage;

import java.util.InputMismatchException;
import java.util.Map;
import java.util.HashMap;

public class Tile implements Drawable {
    public Polygon poly;
    private PImage texture;
    private String texturePath;
    private Polygon drawPoly;
    private boolean isMagnetic;
    private double fieldStrength;
    private int tileNum;
    

    /* TODO: refactor to Texture class? */
    static Map <String, PImage> textureCache = new HashMap <String, PImage> ();
    static PImage getTexture(Window w, String path) {
        PImage img = textureCache.get(path);
        if (img == null) {
            img = w.loadImage(path);
            if (img == null) {
                throw new RuntimeException("Could not load texture \"" + path + "\"");
            }
            textureCache.put(path, img);
        }
        return img;
    }

    public Tile(Polygon poly, PImage texture, String texturePath) {
        this.poly = poly;
        this.texture = texture;
        this.texturePath = texturePath;
        drawPoly = new Polygon(new Vector2D(0,0), new Vector2D(0,0), new Vector2D(0, 1), 
        						new Vector2D(1, 1), new Vector2D(1, 0));
    }
    
    public Tile(Polygon poly2, PImage texture2, String texturePath2,
			boolean _isMagnetic, double _fieldStrength, int tileNum) {
    	this(poly2, texture2, texturePath2);
    	isMagnetic = _isMagnetic;
    	fieldStrength = _fieldStrength;
    	this.tileNum = tileNum;
	}
    
    public int getTileNumber() {
    	return tileNum;
    }
    
    public boolean isMagnetic() {
    	return isMagnetic;
    }
    
    public double fieldStrength() {
    	return fieldStrength;
    }

	public void changeTexture(Window w, String path) {
    	texture = getTexture(w, path);
    }

    @Override
    public void draw(Window w, double dt) {
        if (texture == null && poly != null) {
            poly.draw(w, dt);
        } else {
            w.noStroke();
            w.textureMode(w.NORMAL);
            w.beginShape();
            w.texture(texture);
            for (Vector2D v : drawPoly) {
                Vector2D displayP = v.c().add(poly.pos()).worldToScreen(w);
                w.vertex((float) displayP.x, w.height - (float) displayP.y,
                         v.x(), -v.y() + 1f);
            }
            w.endShape(PApplet.CLOSE);

            // debug collision code
            if (poly != null && poly.collided != null) {
                for (int i = 0; i < poly.collided.size(); ++i) {
                    if (poly.collided.get(i)) {
                        Vector2D v1 = poly.getPoints().get(i).c().add(poly.pos()).worldToScreen(w);
                        Vector2D v2 = poly.getPoints().get((i+1) % poly.getPoints().size()).c().add(poly.pos()).worldToScreen(w);
                        w.stroke(0xffff0000);
                        //w.line((float)v1.x, w.height - (float)v1.y, (float)v2.x, w.height - (float)v2.y);
                    }
                }
            }
        }
    }

    public Tile move(Vector2D dp) {
        return new Tile(poly.move(dp), texture, texturePath, isMagnetic, fieldStrength, tileNum);
    }
    
    public Vector2D pos() {
    	return poly.pos();
    }


    public void store(StringBuffer buf) {
        buf.append("[t \"" + texturePath + "\" ");
        if (poly != null) {
        	poly.store(buf);
        }
        buf.append(" ");
        buf.append(isMagnetic);
        buf.append(" ");
        buf.append(fieldStrength);
        buf.append(" ]");
    }

    static public Tile load(Window w, java.util.Scanner scanner) {
        String head = scanner.next();
        if (head.equals("-")) {
            return null;
        }

        if (!head.equals("[t")) {
            throw new InputMismatchException();
        }

        // hacky string scanning
        String texturePath = "";
        while (texturePath.isEmpty() || texturePath.charAt(texturePath.length() - 1) != '"') {
            texturePath += scanner.next();
        }
        texturePath = texturePath.substring(1, texturePath.length() - 1);

        // polygon (null if fails)
        String nhead = scanner.next();
        Polygon poly = Polygon.load(scanner, nhead);
        
        // magnet
        boolean _isMagnetic = scanner.nextBoolean();
        double _fieldStrength = scanner.nextDouble();
        

        if (!scanner.next().equals("]")) {
            throw new InputMismatchException();
        }

        PImage texture = getTexture(w, texturePath);
        System.out.println("loaded tile: " + texturePath + " " + _isMagnetic + " " + _fieldStrength);
        return new Tile(poly, texture, texturePath, _isMagnetic, _fieldStrength, 0);
    }

    @Override
    public Rect boundingRect() {
        return poly.boundingRect();
    }

	public void setTileNumber(int count) {
		tileNum = count;
	}
}
