import java.util.ArrayList;
import java.util.List;

import processing.core.PApplet;
import processing.core.PImage;


public class Magnus extends Polygon {

    private List<PImage> walkCycler[];
    private List<PImage> walkCyclel[];
    private List<PImage> standCycleR[];
    private List<PImage> standCycleL[];
    private PImage glowN;
    private PImage glowS;
    private int movingFrame;
    private int standFrame;
    private boolean facingRight;
    private Polygon displayPoly;
    private double pos_acc;
    private static final double distance_step = 30;
    private boolean moving;
    private boolean onGround;
    private double time_acc;
    private static final double TIME_STEP = 0.14;
    public static final double SCALE_FACTOR = 1.5;
    private Polygon foot;
    private Polygon displayTexCoords;	
	
	public Magnus (Vector2D pos, Window w) {
		super(pos, 
				 V(0.8, 0), V(0.8, 1), V(0.2, 1), V(0.2, 0));
		displayPoly = new Polygon(V(0,0), V(1, 0), V(1, 1), V(0, 1), V(0, 0));
		foot = new Polygon(V(0.5, -0.5), V(-.1, 0), V(-.1, 0.1), V(.1, 0.1), V(.1, 0));
		displayTexCoords = new Polygon(V(0,0), V(1, 0), V(1, 1), V(0, 1), V(0, 0));
		
		for (Vector2D p : getPoints()) {
			p.x -= 0.5;
		}
		for (Vector2D p : foot) {
			p.x -= 0.5;
		}
		for (Vector2D p : displayPoly) {
			p.x -= 0.5;
		}
		
		// scale factor
		for (Vector2D p : this) {
			p.scale(SCALE_FACTOR);
		}
		for (Vector2D p : displayPoly) {
			p.scale(SCALE_FACTOR);
		}
		for (Vector2D p : foot) {
			p.scale(SCALE_FACTOR);
		}
		
		walkCycler = new List[3];
		walkCyclel = new List[3];
		standCycleR = new List[3];
		standCycleL = new List[3];
		
		
		// load textures for non-glowing
		walkCycler[0] = new ArrayList<PImage>();
		walkCyclel[0] = new ArrayList<PImage>();
		standCycleR[0] = new ArrayList<PImage>();
		standCycleL[0] = new ArrayList<PImage>();
		walkCycler[0].add(Tile.getTexture(w, "magnes_walk1.png"));
		walkCycler[0].add(Tile.getTexture(w, "magnes_walk2.png"));
		walkCycler[0].add(Tile.getTexture(w, "magnes_walk3.png"));
		walkCycler[0].add(Tile.getTexture(w, "magnes_walk4.png"));
		for (PImage i : walkCycler[0]) {
			walkCyclel[0].add(w.imageMirrorX(i));
		}
		standCycleR[0].add(Tile.getTexture(w, "magnes_standRight1.png"));
		standCycleR[0].add(Tile.getTexture(w, "magnes_standRight2.png"));
		standCycleR[0].add(Tile.getTexture(w, "magnes_standRight3.png"));
		standCycleR[0].add(Tile.getTexture(w, "magnes_standRight4.png"));
		for (PImage i : standCycleR[0]) {
			standCycleL[0].add(w.imageMirrorX(i));
		}
		
		// load textures for north glowing
		walkCycler[1] = new ArrayList<PImage>();
		walkCyclel[1] = new ArrayList<PImage>();
		standCycleR[1] = new ArrayList<PImage>();
		standCycleL[1] = new ArrayList<PImage>();
		walkCycler[1].add(Tile.getTexture(w, "magnes_walk1_N.png"));
		walkCycler[1].add(Tile.getTexture(w, "magnes_walk2_N.png"));
		walkCycler[1].add(Tile.getTexture(w, "magnes_walk3_N.png"));
		walkCycler[1].add(Tile.getTexture(w, "magnes_walk4_N.png"));
		for (PImage i : walkCycler[1]) {
			walkCyclel[1].add(w.imageMirrorX(i));
		}
		standCycleR[1].add(Tile.getTexture(w, "magnes_standRight1_N.png"));
		standCycleR[1].add(Tile.getTexture(w, "magnes_standRight2_N.png"));
		standCycleR[1].add(Tile.getTexture(w, "magnes_standRight3_N.png"));
		standCycleR[1].add(Tile.getTexture(w, "magnes_standRight4_N.png"));
		for (PImage i : standCycleR[1]) {
			standCycleL[1].add(w.imageMirrorX(i));
		}
		
		// load textures for south glowing
		walkCycler[2] = new ArrayList<PImage>();
		walkCyclel[2] = new ArrayList<PImage>();
		standCycleR[2] = new ArrayList<PImage>();
		standCycleL[2] = new ArrayList<PImage>();
		walkCycler[2].add(Tile.getTexture(w, "magnes_walk1_S.png"));
		walkCycler[2].add(Tile.getTexture(w, "magnes_walk2_S.png"));
		walkCycler[2].add(Tile.getTexture(w, "magnes_walk3_S.png"));
		walkCycler[2].add(Tile.getTexture(w, "magnes_walk4_S.png"));
		for (PImage i : walkCycler[2]) {
			walkCyclel[2].add(w.imageMirrorX(i));
		}
		standCycleR[2].add(Tile.getTexture(w, "magnes_standRight1_S.png"));
		standCycleR[2].add(Tile.getTexture(w, "magnes_standRight2_S.png"));
		standCycleR[2].add(Tile.getTexture(w, "magnes_standRight3_S.png"));
		standCycleR[2].add(Tile.getTexture(w, "magnes_standRight4_S.png"));
		for (PImage i : standCycleR[2]) {
			standCycleL[2].add(w.imageMirrorX(i));
		}
		
		movingFrame = 0;
		standFrame = 0;
		facingRight = true;
		
		pos_acc = 0;
		time_acc = 0;
		
		moving = true;
		onGround = true;
	}
	
	private static Vector2D V(double x, double y) {
        return new Vector2D(x, y);
    }
	
	public Polygon getGlobalFoot() {
		return foot.move(pos());
	}
	
	@Override
	public void draw(Window w, double dt) {
		if (facingRight && vel().x < 0) {
			facingRight = false;
		} else if (!facingRight && vel().x > 0) {
			facingRight = true;
		}
		
		// work out glow to draw
        int glow = isMagnetic() ? (getMagneticStrength() > 0 ? 1 : 2) : 0;
		
		// use an euler integrator to work out how far we've come
		// this is for the moving animation
		pos_acc += vel().x;
		while (onGround == true && Math.abs(pos_acc) > distance_step) {
			movingFrame = (movingFrame + 1) % walkCycler[glow].size();
			pos_acc += pos_acc > 0 ? -distance_step : distance_step;
		}
		// standing animation
		time_acc += dt;
		while (time_acc > TIME_STEP) {
			standFrame = (standFrame + 1) % standCycleR[glow].size();
			time_acc -= TIME_STEP;
		}
		
		w.noStroke();
        w.textureMode(w.NORMAL);
        w.textureWrap(w.CLAMP);        
        
        // draw magnes himself
        w.beginShape();
        //TODO make sure this is working
        if (this.isMoving() == false && onGround == true) {
        	// Play standing loop
        	w.texture(facingRight ? standCycleR[glow].get(standFrame) : standCycleL[glow].get(standFrame));
        } else {
        	// Play run loop
        	w.texture(facingRight ? walkCycler[glow].get(movingFrame) : walkCyclel[glow].get(movingFrame));
        }
        int i = 0;
        for (Vector2D v : displayPoly) {
            Vector2D displayP = v.c().add(pos()).worldToScreen(w);
            w.vertex((float) displayP.x, w.height - (float) displayP.y,
            		displayTexCoords.getPoints().get(i).x(), 1 - displayTexCoords.getPoints().get(i).y());
            ++i;
        }
        
        w.endShape(PApplet.CLOSE);
        w.stroke(200);
        
        //super.draw(w, dt);
	}

        public void updateRestingOnPlatform(List<Tile> platforms, int width, int height) {
		boolean resting = false;
		Polygon glob = getGlobalFoot();
                Rect br = glob.boundingRect();
                br.ax = Math.floor(br.ax);
                br.ay = Math.floor(br.ay);
                br.bx = Math.ceil(br.bx);
                br.by = Math.ceil(br.by);

                int xstart = Math.max(0, (int)br.ax);
                int xend = Math.min(width, (int)br.bx);
                int ystart = Math.max(0, (int)br.ay);
                int yend = Math.min(height, (int)br.by);
                for (int y = ystart; y < yend && !resting; ++y) {
                    for (int x = xstart; x < xend && !resting; ++x) {
                        Tile t = platforms.get(y * width + x);
                        if (t == null) continue;

                        CollisionResult c = glob.isColliding(t.poly);
                        if (c.collided) {
                            resting = true;
                        }
                    }
                }
		onGround = resting;
	}

    public boolean onPlatform() {
        return onGround;
    }
	
	public static Magnus load(java.util.Scanner scanner, Window w) {
		Polygon p = Polygon.load(scanner);
		return new Magnus(p.pos(), w);
	}
	
	public boolean isMoving() {
		return this.moving;
	}
	
	public void setMoving(boolean m) {
		this.moving = m;
	}
}
