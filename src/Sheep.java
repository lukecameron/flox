import java.util.ArrayList;
import java.util.List;

import processing.core.PApplet;
import processing.core.PImage;


public class Sheep extends Polygon {

    private List<PImage> standCycle;
    private List<PImage> foundCycle;
    private int standFrame;
    private Polygon displayPoly;

    private double time_acc;
    private static final double TIME_STEP = 0.14;
    private Polygon displayTexCoords;	
    private Polygon foot;
    
    private boolean found = false;
	
	
	public Sheep (Vector2D pos, Window w) {
		super(pos, 
				 V(0.8, 0), V(0.8, 0.4), V(0.2, 0.4), V(0.2, 0)); 
		displayPoly = new Polygon(V(0,0), V(1, 0), V(1, 1), V(0, 1), V(0, 0));
		foot = new Polygon(V(0.5, -0.5), V(-.1, 0), V(-.1, 0.1), V(.1, 0.1), V(.1, 0));
		displayTexCoords = new Polygon(V(0,0), V(1, 0), V(1, 1), V(0, 1), V(0, 0));
		
		// scale factor
		double scaleFactor = 2;
		for (Vector2D p : this) {
			p.scale(scaleFactor);
		}
		for (Vector2D p : displayPoly) {
			p.scale(scaleFactor);
		}
		for (Vector2D p : foot) {
			p.scale(scaleFactor);
		}
		
		
		standCycle = new ArrayList<PImage>();
		standCycle.add(Tile.getTexture(w, "sheep1.png"));
		standCycle.add(Tile.getTexture(w, "sheep2.png"));
		standCycle.add(Tile.getTexture(w, "sheep3.png"));
		standCycle.add(Tile.getTexture(w, "sheep4.png"));
		
		foundCycle = new ArrayList<PImage>();
		foundCycle.add(Tile.getTexture(w, "sheep_happy1.png"));
		foundCycle.add(Tile.getTexture(w, "sheep_happy2.png"));
		foundCycle.add(Tile.getTexture(w, "sheep_happy3.png"));
		foundCycle.add(Tile.getTexture(w, "sheep_happy4.png"));
		
		standFrame = 0;
		
		time_acc = 0;
		
	}
	
	public void setFound(boolean isFound) {
		found = isFound;
	}
	
	public boolean isFound() {
		return found;
	}
	
	private static Vector2D V(double x, double y) {
        return new Vector2D(x, y);
    }
	
	public Polygon getGlobalFoot() {
		return foot.move(pos());
	}
	
	@Override
	public void draw(Window w, double dt) {
		
		// standing animation
		time_acc += dt;
		while (time_acc > TIME_STEP) {
			standFrame = (standFrame + 1) % standCycle.size();
			time_acc -= TIME_STEP;
		}
		
		
		w.noStroke();
        w.textureMode(w.NORMAL);
        w.beginShape();
        
        // decide which texture to display
        
    	// Play standing loop
    	w.texture((found ? foundCycle : standCycle).get(standFrame));
    	
    	int i = 0;
        for (Vector2D v : displayPoly) {
            Vector2D displayP = v.c().add(pos()).worldToScreen(w);
            w.vertex((float) displayP.x, w.height - (float) displayP.y,
            		displayTexCoords.getPoints().get(i).x(), 1 - displayTexCoords.getPoints().get(i).y());
            ++i;
        }
        
        w.endShape(PApplet.CLOSE);
        w.stroke(200);
        
        //super.draw(w, dt);
	}
	
	
	public static Sheep load(java.util.Scanner scanner, Window w) {
		Polygon p = Polygon.load(scanner);
		return new Sheep(p.pos(), w);
	}
}
