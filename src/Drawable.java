import processing.core.PApplet;


public interface Drawable {
    public void draw(Window ctx, double dt);
    public Rect boundingRect();
}
