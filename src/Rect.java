// representation of a rectangle
class Rect {
    // a: low x,y; b: high x,y
    public double ax, ay, bx, by;

    Rect(double ax_, double ay_, double bx_, double by_) {
        if (ax > bx || ay > by) {
            throw new RuntimeException("Invalid Rect");
        }
        ax = ax_;
        ay = ay_;
        bx = bx_;
        by = by_;
    }

    public Rect clone() {
        return new Rect(ax, ay, bx, by);
    }

    public Rect move(double x, double y) {
        ax += x;
        ay += y;
        bx += x;
        by += y;
        return this;
    }

    public boolean intersect(Rect b) {
        return !(b.ax >= bx || ax >= b.bx || b.ay >= by || ay >= b.by);
    }

    public boolean top_near(Rect b, double eps) {
        return ax < b.bx && b.ax < bx && by <= b.ay && by > b.ay - eps;
    }

    public boolean bottom_near(Rect b, double eps) {
        return ax < b.bx && b.ax < bx && ay >= b.by && ay < b.by + eps;
    }

    public boolean right_near(Rect b, double eps) {
        return ay < b.by && b.ay < by && bx <= b.ax && bx > b.ax - eps;
    }

    public boolean left_near(Rect b, double eps) {
        return ay < b.by && b.ay < by && ax >= b.bx && ax < b.ax + eps;
    }

    public Rect scale(double xscale, double yscale) {
        if (xscale < 0) {
            xscale = -xscale;
        }
        if (yscale < 0) {
            yscale = -yscale;
        }
        ax *= xscale;
        ay *= yscale;
        bx *= xscale;
        by *= yscale;
        return this;
    }

    public Rect scaleCentered(double xscale, double yscale) {
        double xc = (ax + bx) / 2, yc = (ay + by) / 2;
        return move(-xc, -yc).scale(xscale, yscale).move(xc, yc);
    }

    public double width() {
        return bx - ax;
    }

    public double height() {
        return by - ay;
    }
    
    public Vector2D centre() {
    	return new Vector2D(ax + bx / 2, ay + by / 2);
    }

    public String str() {
        return "(" + ax + "," + ay + " -- " + bx + "," + by + ")";
    }

	public void moveTowardsKeepingAspect(Rect target, double speed) {
            if (speed < 0) speed = 0;
            if (speed > 1) speed = 1;
            ax = ax * (1 - speed) + target.ax * speed;
            ay = ay * (1 - speed) + target.ay * speed;
            bx = bx * (1 - speed) + target.bx * speed;
            by = by * (1 - speed) + target.by * speed;
	}
}
