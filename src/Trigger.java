// trigger that displays text to the player
public class Trigger {
    // area of trigger
    // TODO: should this be a Polygon?
    public Rect area;
    // text to display. Empty string means to clear any existing text.
    public String text;
    // only trigger once
    public boolean once;
    // already triggered (with once)
    public boolean triggered;
    // pause the game, forcing the player to read the text
    // In the current implementation, you probably do not want to use
    // "repeat" and "pause" (!once && forcePause) at the same time.
    public boolean forcePause;
    // remove text after several seconds. 0 means show forever.
    public double removeAfter;

    Trigger(Rect area, String text, boolean once, boolean forcePause, double removeAfter) {
        this.area = area;
        this.text = text;
        this.once = once;
        this.forcePause = forcePause;
        this.removeAfter = removeAfter;

        triggered = false;
    }

    public void store(StringBuffer buf) {
        buf.append("[trigger: ");
        new Vector2D(area.ax, area.ay).store(buf);
        buf.append(" ");
        new Vector2D(area.bx, area.by).store(buf);
        buf.append(" ");
        buf.append("\"" + text.replace("\"", "\\\"").replace("\n", "\\n") + "\"");
        buf.append(" ");
        if (once) {
            buf.append("once");
        } else {
            buf.append("repeat");
        }
        buf.append(" ");
        if (forcePause) {
            buf.append("pause");
        } else {
            buf.append("nopause");
        }
        buf.append(" ");
        buf.append(removeAfter);
        buf.append(" ]");
    }

    static public Trigger load(java.util.Scanner scanner) {
        String tok;
        tok = scanner.next();
        if (!tok.equals("[trigger:")) {
            throw new java.util.InputMismatchException();
        }

        Vector2D rect1 = Vector2D.load(scanner);
        Vector2D rect2 = Vector2D.load(scanner);
        // hacky string scanning -- scan until string ends with <"> and not <\">
        String text = "";
        while (text.isEmpty() ||
               text.charAt(text.length() - 1) != '"' ||
               (text.length() >= 2 && text.charAt(text.length() - 2) == '\\')) {
            String tmp = scanner.next();
            if (!text.isEmpty()) {
                text += " ";
            }
            text += tmp;
        }
        text = text.substring(1, text.length() - 1).replace("\\n", "\n");

        boolean once;
        tok = scanner.next();
        if (tok.equals("once")) {
            once = true;
        } else if (tok.equals("repeat")) {
            once = false;
        } else {
            throw new java.util.InputMismatchException();
        }
        boolean forcePause;
        tok = scanner.next();
        if (tok.equals("pause")) {
            forcePause = true;
        } else if (tok.equals("nopause")) {
            forcePause = false;
        } else {
            throw new java.util.InputMismatchException();
        }

        double removeAfter = scanner.nextDouble();

        tok = scanner.next();
        if (!tok.equals("]")) {
            throw new java.util.InputMismatchException();
        }

        return new Trigger(new Rect(rect1.x, rect1.y, rect2.x, rect2.y),
                           text, once, forcePause, removeAfter);
    }
}